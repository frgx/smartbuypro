import {AfterViewInit, Component, OnInit} from '@angular/core';
import { Barcode, BarcodeScanner } from '@capacitor-mlkit/barcode-scanning';
import {AlertController, Platform} from '@ionic/angular';
import {HttpClient} from "@angular/common/http";
import {Root} from "../model/codebar.model";
import {App} from "@capacitor/app";
import {Router} from "@angular/router";
import {DbBarcordeService} from "../services/db-barcorde.service";

type BarcodeBanned = {
  barcode: Barcode,
  isBanned: boolean
}


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  isSupported = false;
  barcodes: BarcodeBanned[] = [];

  constructor(
    private alertController: AlertController,
    private http: HttpClient,
    private platform: Platform,
    private __router: Router,
    private barcodeDb: DbBarcordeService) {}

  ngOnInit() {
    BarcodeScanner.isSupported().then((result) => {
      this.isSupported = result.supported;
    });
    this.platform.backButton.subscribe( next => {
      if(next)
        App.exitApp();
    });
  }

  close(){
    App.exitApp();
  }

  async scan(): Promise<void> {
    const granted = await this.requestPermissions();
    if (!granted) {
      this.presentAlert();
      return;
    }
    const { barcodes } = await BarcodeScanner.scan();
    for (let barcode of barcodes){

      this.http.get<Root>(`https://world.openfoodfacts.org/api/v2/product/${barcode.rawValue}.json`).subscribe( (barcodeHttp: Root)   => {
        let barcodeBanned: BarcodeBanned = {
          barcode : barcode,
          isBanned: (
            // Si l'origine est le pays en question
            this.barcodeDb.starterList.getValue().map( starter => starter.starter).includes(barcode.rawValue.slice(0,3)) ||
            // si une des marques à boycotter
            this.barcodeDb.brandList.getValue().map(brand => brand.brand?.toUpperCase()).includes(<string>barcodeHttp.product.brands?.toUpperCase()) ||
            // si le produit est commercialiser en dans le pays en question
            // (barcodeHttp as Root).product.ecoscore_data.adjustments.origins_of_ingredients.transportation_scores.is !== 0  ||
            // si le produit vient du pays en question
            barcodeHttp.product.ecoscore_data.adjustments.origins_of_ingredients.aggregated_origins.map(origin => origin.origin).filter( origin => this.barcodeDb.countryList.getValue().map(country => country.country).includes(origin)).length > 0
          )
        };
        this.barcodes.push( barcodeBanned );
      }, (error: any) => {
        console.error(error);
        let barcodeBanned: BarcodeBanned = {
          barcode : barcode,
          isBanned: barcode.rawValue.startsWith("729")||
            barcode.rawValue.startsWith("871")
        };
        this.barcodes.push( barcodeBanned );
      });


    }
  }

  async requestPermissions(): Promise<boolean> {
    const { camera } = await BarcodeScanner.requestPermissions();
    return camera === 'granted' || camera === 'limited';
  }

  async presentAlert(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Permission denied',
      message: 'Please grant camera permission to use the barcode scanner.',
      buttons: ['OK'],
    });
    await alert.present();
  }

  goToOptions() {
    this.__router.navigateByUrl("/options");
  }
}
