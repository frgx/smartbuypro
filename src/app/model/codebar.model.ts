export interface Root {
  code: string
  product: Product
  status: number
  status_verbose: string
}

export interface Product {
  _id: string
  _keywords: string[]
  abbreviated_product_name: string
  abbreviated_product_name_fr: string
  abbreviated_product_name_fr_imported: string
  added_countries_tags: any[]
  additives_n: number
  additives_old_n: number
  additives_old_tags: string[]
  additives_original_tags: string[]
  additives_tags: string[]
  allergens: string
  allergens_from_ingredients: string
  allergens_from_user: string
  allergens_hierarchy: any[]
  allergens_tags: any[]
  amino_acids_tags: any[]
  brands: string
  brands_imported: string
  brands_tags: string[]
  categories_properties: CategoriesProperties
  categories_properties_tags: string[]
  checkers_tags: any[]
  code: string
  codes_tags: string[]
  complete: number
  completeness: number
  conservation_conditions: string
  conservation_conditions_fr: string
  conservation_conditions_fr_imported: string
  correctors_tags: string[]
  countries: string
  countries_hierarchy: string[]
  countries_imported: string
  countries_tags: string[]
  created_t: number
  creator: string
  customer_service: string
  customer_service_fr: string
  customer_service_fr_imported: string
  data_quality_bugs_tags: any[]
  data_quality_errors_tags: any[]
  data_quality_info_tags: string[]
  data_quality_tags: string[]
  data_quality_warnings_tags: string[]
  data_sources: string
  data_sources_imported: string
  data_sources_tags: string[]
  ecoscore_data: EcoscoreData
  ecoscore_grade: string
  ecoscore_tags: string[]
  editors_tags: string[]
  entry_dates_tags: string[]
  food_groups_tags: any[]
  generic_name: string
  generic_name_fr: string
  generic_name_fr_imported: string
  id: string
  image_front_small_url: string
  image_front_thumb_url: string
  image_front_url: string
  image_nutrition_small_url: string
  image_nutrition_thumb_url: string
  image_nutrition_url: string
  image_small_url: string
  image_thumb_url: string
  image_url: string
  images: Images
  informers_tags: string[]
  ingredients: Ingredient[]
  ingredients_analysis: IngredientsAnalysis
  ingredients_analysis_tags: string[]
  ingredients_from_or_that_may_be_from_palm_oil_n: number
  ingredients_from_palm_oil_n: number
  ingredients_from_palm_oil_tags: any[]
  ingredients_hierarchy: string[]
  ingredients_lc: string
  ingredients_n: number
  ingredients_n_tags: string[]
  ingredients_original_tags: string[]
  ingredients_percent_analysis: number
  ingredients_tags: string[]
  ingredients_text: string
  ingredients_text_fr: string
  ingredients_text_fr_imported: string
  ingredients_text_with_allergens: string
  ingredients_text_with_allergens_fr: string
  ingredients_that_may_be_from_palm_oil_n: number
  ingredients_that_may_be_from_palm_oil_tags: any[]
  ingredients_with_specified_percent_n: number
  ingredients_with_specified_percent_sum: number
  ingredients_with_unspecified_percent_n: number
  ingredients_with_unspecified_percent_sum: number
  ingredients_without_ciqual_codes: string[]
  ingredients_without_ciqual_codes_n: number
  interface_version_created: string
  interface_version_modified: string
  known_ingredients_n: number
  labels: string
  labels_hierarchy: string[]
  labels_imported: string
  labels_lc: string
  labels_tags: string[]
  lang: string
  lang_imported: string
  languages: Languages
  languages_codes: LanguagesCodes
  languages_hierarchy: string[]
  languages_tags: string[]
  last_edit_dates_tags: string[]
  last_editor: string
  last_image_dates_tags: string[]
  last_image_t: number
  last_modified_by: string
  last_modified_t: number
  lc: string
  lc_imported: string
  main_countries_tags: any[]
  max_imgid: string
  minerals_tags: any[]
  misc_tags: string[]
  nova_group: number
  nova_group_debug: string
  nova_groups: string
  nova_groups_markers: NovaGroupsMarkers
  nova_groups_tags: string[]
  nucleotides_tags: any[]
  nutrient_levels: NutrientLevels
  nutrient_levels_tags: any[]
  nutriments: Nutriments
  nutriscore: Nutriscore
  nutriscore_2021_tags: string[]
  nutriscore_2023_tags: string[]
  nutriscore_grade: string
  nutriscore_tags: string[]
  nutriscore_version: string
  nutrition_data: string
  nutrition_data_per: string
  nutrition_data_per_imported: string
  nutrition_data_prepared_per: string
  nutrition_data_prepared_per_imported: string
  nutrition_grade_fr: string
  nutrition_grades: string
  nutrition_grades_tags: string[]
  nutrition_score_beverage: number
  nutrition_score_debug: string
  nutrition_score_warning_fruits_vegetables_legumes_estimate_from_ingredients: number
  nutrition_score_warning_fruits_vegetables_legumes_estimate_from_ingredients_value: number
  nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients: number
  nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients_value: number
  obsolete_imported: string
  other_nutritional_substances_tags: any[]
  owner: string
  owner_fields: OwnerFields
  owners_tags: string
  packaging_materials_tags: any[]
  packaging_recycling_tags: any[]
  packaging_shapes_tags: any[]
  packagings: any[]
  packagings_materials: PackagingsMaterials
  photographers_tags: string[]
  pnns_groups_1: string
  pnns_groups_1_tags: string[]
  pnns_groups_2: string
  pnns_groups_2_tags: string[]
  popularity_key: number
  preparation: string
  preparation_fr: string
  preparation_fr_imported: string
  product_name: string
  product_name_fr: string
  product_name_fr_imported: string
  product_quantity: string
  quantity: string
  quantity_imported: string
  removed_countries_tags: any[]
  rev: number
  selected_images: SelectedImages
  serving_quantity: string
  serving_size: string
  serving_size_imported: string
  sources: Source[]
  sources_fields: SourcesFields
  states: string
  states_hierarchy: string[]
  states_tags: string[]
  teams: string
  teams_tags: string[]
  traces: string
  traces_from_ingredients: string
  traces_from_user: string
  traces_hierarchy: any[]
  traces_tags: any[]
  unknown_ingredients_n: number
  unknown_nutrients_tags: any[]
  update_key: string
  vitamins_tags: any[]
  weighers_tags: any[]
  with_non_nutritive_sweeteners: number
  with_sweeteners: number
}

export interface CategoriesProperties {}

export interface EcoscoreData {
  adjustments: Adjustments
  agribalyse: Agribalyse
  missing: Missing
  missing_agribalyse_match_warning: number
  missing_key_data: number
  scores: Scores
  status: string
}

export interface Adjustments {
  origins_of_ingredients: OriginsOfIngredients
  packaging: Packaging
  production_system: ProductionSystem
  threatened_species: ThreatenedSpecies
}

export interface OriginsOfIngredients {
  aggregated_origins: AggregatedOrigin[]
  epi_score: number
  epi_value: number
  origins_from_origins_field: string[]
  transportation_score: number
  transportation_scores: TransportationScores
  transportation_value: number
  transportation_values: TransportationValues
  value: number
  values: Values
  warning: string
}

export interface AggregatedOrigin {
  epi_score: string
  origin: string
  percent: number
  transportation_score: any
}

export interface TransportationScores {
  ad: number
  al: number
  at: number
  ax: number
  ba: number
  be: number
  bg: number
  ch: number
  cy: number
  cz: number
  de: number
  dk: number
  dz: number
  ee: number
  eg: number
  es: number
  fi: number
  fo: number
  fr: number
  gg: number
  gi: number
  gr: number
  hr: number
  hu: number
  ie: number
  il: number
  im: number
  is: number
  it: number
  je: number
  lb: number
  li: number
  lt: number
  lu: number
  lv: number
  ly: number
  ma: number
  mc: number
  md: number
  me: number
  mk: number
  mt: number
  nl: number
  no: number
  pl: number
  ps: number
  pt: number
  ro: number
  rs: number
  se: number
  si: number
  sj: number
  sk: number
  sm: number
  sy: number
  tn: number
  tr: number
  ua: number
  uk: number
  us: number
  va: number
  world: number
  xk: number
}

export interface TransportationValues {
  ad: number
  al: number
  at: number
  ax: number
  ba: number
  be: number
  bg: number
  ch: number
  cy: number
  cz: number
  de: number
  dk: number
  dz: number
  ee: number
  eg: number
  es: number
  fi: number
  fo: number
  fr: number
  gg: number
  gi: number
  gr: number
  hr: number
  hu: number
  ie: number
  il: number
  im: number
  is: number
  it: number
  je: number
  lb: number
  li: number
  lt: number
  lu: number
  lv: number
  ly: number
  ma: number
  mc: number
  md: number
  me: number
  mk: number
  mt: number
  nl: number
  no: number
  pl: number
  ps: number
  pt: number
  ro: number
  rs: number
  se: number
  si: number
  sj: number
  sk: number
  sm: number
  sy: number
  tn: number
  tr: number
  ua: number
  uk: number
  us: number
  va: number
  world: number
  xk: number
}

export interface Values {
  ad: number
  al: number
  at: number
  ax: number
  ba: number
  be: number
  bg: number
  ch: number
  cy: number
  cz: number
  de: number
  dk: number
  dz: number
  ee: number
  eg: number
  es: number
  fi: number
  fo: number
  fr: number
  gg: number
  gi: number
  gr: number
  hr: number
  hu: number
  ie: number
  il: number
  im: number
  is: number
  it: number
  je: number
  lb: number
  li: number
  lt: number
  lu: number
  lv: number
  ly: number
  ma: number
  mc: number
  md: number
  me: number
  mk: number
  mt: number
  nl: number
  no: number
  pl: number
  ps: number
  pt: number
  ro: number
  rs: number
  se: number
  si: number
  sj: number
  sk: number
  sm: number
  sy: number
  tn: number
  tr: number
  ua: number
  uk: number
  us: number
  va: number
  world: number
  xk: number
}

export interface Packaging {
  non_recyclable_and_non_biodegradable_materials: number
  value: number
  warning: string
}

export interface ProductionSystem {
  labels: any[]
  value: number
  warning: string
}

export interface ThreatenedSpecies {}

export interface Agribalyse {
  warning: string
}

export interface Missing {
  categories: number
  labels: number
  origins: number
  packagings: number
}

export interface Scores {}

export interface Images {
  "1": N1
  "10": N10
  "11": N11
  "12": N12
  "2": N2
  "3": N3
  "4": N4
  "5": N5
  "6": N6
  "7": N7
  "8": N8
  "9": N9
  front_fr: FrontFr
  nutrition_fr: NutritionFr
}

export interface N1 {
  sizes: Sizes
  uploaded_t: number
  uploader: string
}

export interface Sizes {
  "100": N100
  "400": N400
  full: Full
}

export interface N100 {
  h: number
  w: number
}

export interface N400 {
  h: number
  w: number
}

export interface Full {
  h: number
  w: number
}

export interface N10 {
  sizes: Sizes2
  uploaded_t: number
  uploader: string
}

export interface Sizes2 {
  "100": N1002
  "400": N4002
  full: Full2
}

export interface N1002 {
  h: number
  w: number
}

export interface N4002 {
  h: number
  w: number
}

export interface Full2 {
  h: number
  w: number
}

export interface N11 {
  sizes: Sizes3
  uploaded_t: number
  uploader: string
}

export interface Sizes3 {
  "100": N1003
  "400": N4003
  full: Full3
}

export interface N1003 {
  h: number
  w: number
}

export interface N4003 {
  h: number
  w: number
}

export interface Full3 {
  h: number
  w: number
}

export interface N12 {
  sizes: Sizes4
  uploaded_t: number
  uploader: string
}

export interface Sizes4 {
  "100": N1004
  "400": N4004
  full: Full4
}

export interface N1004 {
  h: number
  w: number
}

export interface N4004 {
  h: number
  w: number
}

export interface Full4 {
  h: number
  w: number
}

export interface N2 {
  sizes: Sizes5
  uploaded_t: number
  uploader: string
}

export interface Sizes5 {
  "100": N1005
  "400": N4005
  full: Full5
}

export interface N1005 {
  h: number
  w: number
}

export interface N4005 {
  h: number
  w: number
}

export interface Full5 {
  h: number
  w: number
}

export interface N3 {
  sizes: Sizes6
  uploaded_t: number
  uploader: string
}

export interface Sizes6 {
  "100": N1006
  "400": N4006
  full: Full6
}

export interface N1006 {
  h: number
  w: number
}

export interface N4006 {
  h: number
  w: number
}

export interface Full6 {
  h: number
  w: number
}

export interface N4 {
  sizes: Sizes7
  uploaded_t: number
  uploader: string
}

export interface Sizes7 {
  "100": N1007
  "400": N4007
  full: Full7
}

export interface N1007 {
  h: number
  w: number
}

export interface N4007 {
  h: number
  w: number
}

export interface Full7 {
  h: number
  w: number
}

export interface N5 {
  sizes: Sizes8
  uploaded_t: number
  uploader: string
}

export interface Sizes8 {
  "100": N1008
  "400": N4008
  full: Full8
}

export interface N1008 {
  h: number
  w: number
}

export interface N4008 {
  h: number
  w: number
}

export interface Full8 {
  h: number
  w: number
}

export interface N6 {
  sizes: Sizes9
  uploaded_t: number
  uploader: string
}

export interface Sizes9 {
  "100": N1009
  "400": N4009
  full: Full9
}

export interface N1009 {
  h: number
  w: number
}

export interface N4009 {
  h: number
  w: number
}

export interface Full9 {
  h: number
  w: number
}

export interface N7 {
  sizes: Sizes10
  uploaded_t: number
  uploader: string
}

export interface Sizes10 {
  "100": N10010
  "400": N40010
  full: Full10
}

export interface N10010 {
  h: number
  w: number
}

export interface N40010 {
  h: number
  w: number
}

export interface Full10 {
  h: number
  w: number
}

export interface N8 {
  sizes: Sizes11
  uploaded_t: number
  uploader: string
}

export interface Sizes11 {
  "100": N10011
  "400": N40011
  full: Full11
}

export interface N10011 {
  h: number
  w: number
}

export interface N40011 {
  h: number
  w: number
}

export interface Full11 {
  h: number
  w: number
}

export interface N9 {
  sizes: Sizes12
  uploaded_t: number
  uploader: string
}

export interface Sizes12 {
  "100": N10012
  "400": N40012
  full: Full12
}

export interface N10012 {
  h: number
  w: number
}

export interface N40012 {
  h: number
  w: number
}

export interface Full12 {
  h: number
  w: number
}

export interface FrontFr {
  angle: number
  coordinates_image_size: string
  geometry: string
  imgid: string
  normalize: string
  rev: string
  sizes: Sizes13
  white_magic: string
  x1: string
  x2: string
  y1: string
  y2: string
}

export interface Sizes13 {
  "100": N10013
  "200": N200
  "400": N40013
  full: Full13
}

export interface N10013 {
  h: number
  w: number
}

export interface N200 {
  h: number
  w: number
}

export interface N40013 {
  h: number
  w: number
}

export interface Full13 {
  h: number
  w: number
}

export interface NutritionFr {
  angle: number
  coordinates_image_size: string
  geometry: string
  imgid: string
  normalize: any
  rev: string
  sizes: Sizes14
  white_magic: any
  x1: string
  x2: string
  y1: string
  y2: string
}

export interface Sizes14 {
  "100": N10014
  "200": N2002
  "400": N40014
  full: Full14
}

export interface N10014 {
  h: number
  w: number
}

export interface N2002 {
  h: number
  w: number
}

export interface N40014 {
  h: number
  w: number
}

export interface Full14 {
  h: number
  w: number
}

export interface Ingredient {
  ciqual_food_code?: string
  id: string
  percent_estimate: number
  percent_max: number
  percent_min: number
  text: string
  vegan?: string
  vegetarian?: string
  ingredients?: Ingredient2[]
  percent?: number
}

export interface Ingredient2 {
  id: string
  percent_estimate: number
  percent_max: number
  percent_min: number
  text: string
  vegan?: string
  vegetarian?: string
}

export interface IngredientsAnalysis {
  "en:vegan-status-unknown": string[]
  "en:vegetarian-status-unknown": string[]
}

export interface Languages {
  "en:french": number
}

export interface LanguagesCodes {
  fr: number
}

export interface NovaGroupsMarkers {
  "3": string[][]
  "4": string[][]
}

export interface NutrientLevels {}

export interface Nutriments {
  carbohydrates: number
  carbohydrates_100g: number
  carbohydrates_serving: number
  carbohydrates_unit: string
  carbohydrates_value: number
  energy: number
  "energy-kcal": number
  "energy-kcal_100g": number
  "energy-kcal_serving": number
  "energy-kcal_unit": string
  "energy-kcal_value": number
  "energy-kcal_value_computed": number
  "energy-kj": number
  "energy-kj_100g": number
  "energy-kj_serving": number
  "energy-kj_unit": string
  "energy-kj_value": number
  "energy-kj_value_computed": number
  energy_100g: number
  energy_serving: number
  energy_unit: string
  energy_value: number
  fat: number
  fat_100g: number
  fat_serving: number
  fat_unit: string
  fat_value: number
  fiber: number
  fiber_100g: number
  fiber_serving: number
  fiber_unit: string
  fiber_value: number
  "fruits-vegetables-legumes-estimate-from-ingredients_100g": number
  "fruits-vegetables-legumes-estimate-from-ingredients_serving": number
  "fruits-vegetables-nuts-estimate-from-ingredients_100g": number
  "fruits-vegetables-nuts-estimate-from-ingredients_serving": number
  "nova-group": number
  "nova-group_100g": number
  "nova-group_serving": number
  proteins: number
  proteins_100g: number
  proteins_serving: number
  proteins_unit: string
  proteins_value: number
  salt: number
  salt_100g: number
  salt_serving: number
  salt_unit: string
  salt_value: number
  "saturated-fat": number
  "saturated-fat_100g": number
  "saturated-fat_serving": number
  "saturated-fat_unit": string
  "saturated-fat_value": number
  sodium: number
  sodium_100g: number
  sodium_serving: number
  sodium_unit: string
  sodium_value: number
  sugars: number
  sugars_100g: number
  sugars_serving: number
  sugars_unit: string
  sugars_value: number
}

export interface Nutriscore {
  "2021": N2021
  "2023": N2023
}

export interface N2021 {
  category_available: number
  data: Data
  grade: string
  nutrients_available: number
  nutriscore_applicable: number
  nutriscore_computed: number
}

export interface Data {
  energy: number
  fiber: number
  fruits_vegetables_nuts_colza_walnut_olive_oils: number
  is_beverage: number
  is_cheese: number
  is_fat: number
  is_water: number
  proteins: number
  saturated_fat: number
  sodium: number
  sugars: number
}

export interface N2023 {
  category_available: number
  data: Data2
  grade: string
  nutrients_available: number
  nutriscore_applicable: number
  nutriscore_computed: number
}

export interface Data2 {
  energy: number
  fiber: number
  fruits_vegetables_legumes: number
  is_beverage: number
  is_cheese: number
  is_fat_oil_nuts_seeds: number
  is_red_meat_product: number
  is_water: number
  proteins: number
  salt: number
  saturated_fat: number
  sugars: number
}

export interface OwnerFields {
  abbreviated_product_name_fr: number
  brands: number
  carbohydrates: number
  conservation_conditions_fr: number
  countries: number
  customer_service_fr: number
  data_sources: number
  "energy-kcal": number
  "energy-kj": number
  fat: number
  generic_name_fr: number
  ingredients_text_fr: number
  labels: number
  lang: number
  lc: number
  nutrition_data_per: number
  nutrition_data_prepared_per: number
  obsolete: number
  preparation_fr: number
  product_name_fr: number
  proteins: number
  quantity: number
  salt: number
  "saturated-fat": number
  serving_size: number
  sugars: number
}

export interface PackagingsMaterials {}

export interface SelectedImages {
  front: Front
  nutrition: Nutrition
}

export interface Front {
  display: Display
  small: Small
  thumb: Thumb
}

export interface Display {
  fr: string
}

export interface Small {
  fr: string
}

export interface Thumb {
  fr: string
}

export interface Nutrition {
  display: Display2
  small: Small2
  thumb: Thumb2
}

export interface Display2 {
  fr: string
}

export interface Small2 {
  fr: string
}

export interface Thumb2 {
  fr: string
}

export interface Source {
  fields: string[]
  id: string
  images: any[]
  import_t: number
  manufacturer: number
  name: string
  url: any
}

export interface SourcesFields {
  "org-gs1": OrgGs1
}

export interface OrgGs1 {
  gln: string
  gpcCategoryCode: string
  gpcCategoryName: string
  isAllergenRelevantDataProvided: string
  lastChangeDateTime: string
  partyName: string
  productionVariantDescription: string
  publicationDateTime: string
}
