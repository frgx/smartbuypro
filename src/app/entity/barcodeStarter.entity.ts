import {startWith} from "rxjs";

export class BarcodeStarterEntity  {

    id?: number;
    starter?: string;

  public constructor(startWith: string) {
    this.starter = startWith;
  }
}
