
export  class BarcodeCountryEntity {

    id?: number;
    country?: string;

  public constructor(country: string) {
    this.country = country;
  }
}
