import {capSQLiteUpgradeOptions, SQLiteDBConnection} from '@capacitor-community/sqlite';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

import {SQLiteService} from './sqlite.service';
import {DbnameVersionService} from './dbname-version.service';
import {environment} from 'src/environments/environment';
import {BarcodeStarterEntity} from "../entity/barcodeStarter.entity";
import {BarcodeCountryEntity} from "../entity/barcodeCountry.entity";
import {BarcodeBrandEntity} from "../entity/barcodeBrand.entity";
import {bannedBrand, countryBanned, starterBanned} from "../mock-data/barcode-brand-country";
import {barcodeVersionUpgrades} from "../upgrades/barcode-brand-country/upgrade-statements";


@Injectable()
export class DbBarcordeService {
  public databaseName: string;
  public starterList: BehaviorSubject<BarcodeStarterEntity[]> = new BehaviorSubject<BarcodeStarterEntity[]>([]);
  public countryList: BehaviorSubject<BarcodeCountryEntity[]> = new BehaviorSubject<BarcodeCountryEntity[]>([]);
  public brandList: BehaviorSubject<BarcodeBrandEntity[]> = new BehaviorSubject<BarcodeBrandEntity[]>([]);

  private isStarterReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private isCountryReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private isBrandReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private versionUpgrades = barcodeVersionUpgrades;
  private loadToVersion = this.versionUpgrades[this.versionUpgrades.length - 1].toVersion;
  public mDb!: SQLiteDBConnection;

  constructor(public sqliteService: SQLiteService,
              public dbVerService: DbnameVersionService,
  ) {
    this.databaseName = environment.databaseName;
  }


  async initializeDatabase() {

    // create upgrade statements
    console.log("START create upgrade statements");
    await this.sqliteService
      .addUpgradeStatement({
        database: this.databaseName,
        upgrade: this.versionUpgrades
      } as capSQLiteUpgradeOptions);
    console.log("END create upgrade statements");

    // create and/or open the database
    await this.openDatabase();

    this.dbVerService.set(this.databaseName, this.loadToVersion);

    // for (let i = 0; i < this.versionUpgrades.length; i++) {
    //   for (const statement of (this.versionUpgrades)[i].statements) {
    //     await this.mDb.execute(statement);
    //   }
    // }
    // // create database initial data
    //   await this.createInitialData();
    if (this.sqliteService.platform === 'web') {
      await this.sqliteService.sqliteConnection.saveToStore(this.databaseName);
    }
    await this.getAllData();
  }

  async openDatabase() {
    if ((this.sqliteService.native || this.sqliteService.platform === "electron")
      && (await this.sqliteService.isInConfigEncryption()).result
      && (await this.sqliteService.isDatabaseEncrypted(this.databaseName)).result) {
      this.mDb = await this.sqliteService
        .openDatabase(this.databaseName, true, "secret",
          this.loadToVersion, false);

    } else {
      this.mDb = await this.sqliteService
        .openDatabase(this.databaseName, false, "no-encryption",
          this.loadToVersion, false);
    }
  }

  async getAllData() {
    await this.getAllStarters();
    this.isStarterReady.next(true);
    await this.getAllCountries();
    this.isCountryReady.next(true);
    await this.getAllBrands();
    this.isBrandReady.next(true);
  }

  /**
   * Return Author state
   * @returns
   */
  authorState() {
    return this.isCountryReady.asObservable();
  }

  /**
   * Return Post state
   * @returns
   */
  postState() {
    return this.isStarterReady.asObservable();
  }

  /**
   * Return Ids Sequence state
   * @returns
   */
  idsSeqState() {
    return this.isBrandReady.asObservable();
  }

  /**
   * Fetch Authors
   * @returns
   */
  fetchAuthors(): Observable<BarcodeStarterEntity[]> {
    return this.starterList.asObservable();
  }

  /**
   * Fetch Posts
   * @returns
   */
  fetchPosts(): Observable<BarcodeCountryEntity[]> {
    return this.countryList.asObservable();
  }

  /**
   * Fetch Ids Sequence
   * @returns
   */
  fetchIdsSeq(): Observable<BarcodeBrandEntity[]> {
    return this.brandList.asObservable();
  }

  /**
   * Get, Create, Update a Brand
   * @returns
   */
  async getBrand(brand: BarcodeBrandEntity): Promise<BarcodeBrandEntity> {
    let brandDb = await this.sqliteService.findOneBy(this.mDb, "barcode_brand", {id: brand.id});
    if (!brandDb) {
      if (brand.brand) {
        // create a new brandDb
        brandDb = new BarcodeBrandEntity(brand.brand);
        brandDb.id = brand.id;
        await this.sqliteService.save(this.mDb, "barcode_brand", brandDb);
        brandDb = await this.sqliteService.findOneBy(this.mDb, "barcode_brand", {id: brand.id});
        if (brandDb) {
          this.getAllBrands();
          return brandDb;
        } else {
          this.getAllBrands();
          return Promise.reject(`failed to getBrand for id ${brand.id}`);
        }
      } else {
        // brandDb not in the database
        brandDb = new BarcodeBrandEntity('');
        brandDb.id = -1;
        return brandDb;
      }
    } else {
      if (Object.keys(brand).length > 1) {
        // update and existing brandDb
        const updBrand = new BarcodeBrandEntity('' + brand.brand);
        updBrand.id = brand.id;

        await this.sqliteService.save(this.mDb, "barcode_brand", updBrand, {id: brand.id});
        brandDb = await this.sqliteService.findOneBy(this.mDb, "barcode_brand", {id: brand.id});
        if (brandDb) {
          this.getAllBrands();
          return brandDb;
        } else {
          this.getAllBrands();
          return Promise.reject(`failed to getBrand for id ${brand.id}`);
        }
      } else {
        return brandDb;
      }
    }
  }

  /**
   * Delete a Brand
   * @returns
   */
  async deleteBrand(id: string): Promise<void> {
    let brandDb = await this.sqliteService.findOneBy(this.mDb, "barcode_brand", {id: id});
    if (brandDb) {
      await this.sqliteService.remove(this.mDb, "barcode_brand", {id: id});
    }
    this.getAllBrands();
    return;
  }

  /**
   * Get, Create, Update a Brand
   * @returns
   */
  async getCountry(country: BarcodeCountryEntity): Promise<BarcodeBrandEntity> {
    let countryDb = await this.sqliteService.findOneBy(this.mDb, "barcode_country", {id: country.id});
    if (!countryDb) {
      if (country.country) {
        // create a new brandDb
        countryDb = new BarcodeCountryEntity('');
        countryDb.country = country.country;
        countryDb.id = country.id;
        await this.sqliteService.save(this.mDb, "barcode_country", countryDb);
        countryDb = await this.sqliteService.findOneBy(this.mDb, "barcode_country", {id: country.id});
        if (countryDb) {
          this.getAllCountries();
          return countryDb;
        } else {
          this.getAllCountries();
          return Promise.reject(`failed to getCountry for id ${country.id}`);
        }
      } else {
        // brandDb not in the database
        countryDb = new BarcodeCountryEntity('');
        countryDb.id = -1;
        this.getAllCountries();
        return countryDb;
      }
    } else {
      if (Object.keys(country).length > 1) {
        // update and existing brandDb
        const updCountry = new BarcodeCountryEntity('');
        updCountry.id = country.id;
        updCountry.country = country.country;

        await this.sqliteService.save(this.mDb, "barcode_country", updCountry, {id: country.id});
        countryDb = await this.sqliteService.findOneBy(this.mDb, "barcode_country", {id: country.id});
        if (countryDb) {
          this.getAllCountries();
          return countryDb;
        } else {
          this.getAllCountries();
          return Promise.reject(`failed to getCountry for id ${country.id}`);
        }
      } else {
        this.getAllCountries();
        return countryDb;
      }
    }
  }

  /**
   * Delete a Brand
   * @returns
   */
  async deleteCountry(id: string): Promise<void> {
    let countryDb = await this.sqliteService.findOneBy(this.mDb, "barcode_country", {id: id});
    console.log(countryDb);
    if (countryDb) {
      await this.sqliteService.remove(this.mDb, "barcode_country", {id: id});
    }
    this.getAllCountries()
    return;
  }

  /**
   * Get, Create, Update a Starter
   * @returns
   */
  async getStarter(starter: BarcodeStarterEntity): Promise<BarcodeBrandEntity> {
    let starterDb = await this.sqliteService.findOneBy(this.mDb, "barcode_starter", {id: starter.id});
    if (!starterDb) {
      if (starter.starter) {
        // create a new brandDb
        starterDb = new BarcodeStarterEntity('');
        starterDb.startWith = starter.starter;
        starterDb.id = starter.id;
        await this.sqliteService.save(this.mDb, "barcode_starter", starterDb);
        starterDb = await this.sqliteService.findOneBy(this.mDb, "barcode_starter", {id: starter.id});
        if (starterDb) {
          this.getAllStarters();
          return starterDb;
        } else {
          this.getAllStarters();
          return Promise.reject(`failed to getStarter for id ${starter.id}`);
        }
      } else {
        // brandDb not in the database
        starterDb = new BarcodeBrandEntity('');
        starterDb.id = -1;
        return starterDb;
      }
    } else {
      if (Object.keys(starter).length > 1) {
        // update and existing brandDb
        const updStarter = new BarcodeStarterEntity('');
        updStarter.id = starter.id;
        updStarter.starter = starter.starter;

        await this.sqliteService.save(this.mDb, "barcode_starter", updStarter, {id: starter.id});
        starterDb = await this.sqliteService.findOneBy(this.mDb, "barcode_starter", {id: starter.id});
        if (starterDb) {
          this.getAllStarters();
          return starterDb;
        } else {
          this.getAllStarters();
          return Promise.reject(`failed to getStarter for id ${starter.id}`);
        }
      } else {
        this.getAllStarters();
        return starterDb;
      }
    }
  }

  /**
   * Delete a Starter
   * @returns
   */
  async deleteStarter(id: string): Promise<void> {
    let starterDb = await this.sqliteService.findOneBy(this.mDb, "barcode_starter", {id: id});
    if (starterDb) {
      await this.sqliteService.remove(this.mDb, "barcode_starter", {id: id});
    }
    this.getAllStarters();
    return;
  }

  /**
   * Get all Authors
   * @returns
   */
  async getAllStarters(): Promise<void> {
    const starters: BarcodeStarterEntity[] = (await this.mDb.query("select * from barcode_starter")).values as BarcodeStarterEntity[];
    this.starterList.next(starters);
  }


  /**
   * Get all Posts
   * @returns
   */
  async getAllCountries(): Promise<void> {
    const countries: BarcodeCountryEntity[] = (await this.mDb.query("select * from barcode_country")).values as BarcodeCountryEntity[];
    // add the posts to the postList
    this.countryList.next(countries);
  }

  /**
   * Get
   * all Ids Sequence
   * @returns
   */
  async getAllBrands(): Promise<void> {
    const brands: BarcodeBrandEntity[] = (await this.mDb.query("select * from barcode_brand")).values as BarcodeBrandEntity[];
    this.brandList.next(brands);
  }


  /*********************
   * Private Functions *
   *********************/

  /**
   * Create Database Initial Data
   * @returns
   */
  private async createInitialData(): Promise<void> {
    let isData = await this.mDb.query("select * from barcode_brand");
    if (isData?.values?.length === 0)
      // create brands
      for (const brand of bannedBrand) {
        let barcodeBrand = new BarcodeBrandEntity('');
        barcodeBrand.brand = brand
        await this.saveBrand(barcodeBrand);
      }
    isData = await this.mDb.query("select * from barcode_country");
    if (isData?.values?.length === 0)
      // create countries
      for (const country of countryBanned) {
        let barcodeCountry = new BarcodeCountryEntity('');
        barcodeCountry.country = country
        await this.saveCountry(barcodeCountry);

      }
    isData = await this.mDb.query("select * from barcode_starter");
    if (isData?.values?.length === 0)
      // create posts
      for (const startWith of starterBanned) {
        let barcodeStarter = new BarcodeStarterEntity(startWith);
        barcodeStarter.starter = startWith
        await this.saveStarter(barcodeStarter);

      }
  }

  async saveStarter(barcodeStarter: BarcodeStarterEntity) {
    await this.sqliteService.save(this.mDb, "barcode_starter", barcodeStarter);
    await this.getAllStarters();
  }

  async saveCountry(barcodeCountry: BarcodeCountryEntity) {
    await this.sqliteService.save(this.mDb, "barcode_country", barcodeCountry);
    await this.getAllCountries();
  }

  async saveBrand(barcodeBrand: BarcodeBrandEntity) {
    await this.sqliteService.save(this.mDb, "barcode_brand", barcodeBrand);
    await this.getAllBrands();
  }
}
