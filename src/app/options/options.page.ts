import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {DbBarcordeService} from "../services/db-barcorde.service";
import {BarcodeBrandEntity} from "../entity/barcodeBrand.entity";
import {BarcodeCountryEntity} from "../entity/barcodeCountry.entity";
import {BarcodeStarterEntity} from "../entity/barcodeStarter.entity";
import {countryBanned} from "../mock-data/barcode-brand-country";

@Component({
  selector: 'app-options',
  templateUrl: './options.page.html',
  styleUrls: ['./options.page.scss'],
})
export class OptionsPage implements OnInit {
  public marque: string = '';
  public debut: string = '';
  public country: string = '';
  constructor(private __router : Router,
              public db: DbBarcordeService) { }

  ngOnInit() {
  }

  goToHome() {
    this.__router.navigateByUrl("/home");
  }

  newBarcodeBrand(brand: string){
    return new BarcodeBrandEntity(brand);
  }

  newBarcodeCountry(country: string){
    return new BarcodeCountryEntity(country);
  }

  newBarcodeStarter(starter: string){
    return new BarcodeStarterEntity(starter);
  }


}
