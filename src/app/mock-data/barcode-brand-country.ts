import {BarcodeBrandEntity} from "../entity/barcodeBrand.entity";

export const bannedBrand: string[] =[
  // Marques du pays en question
  "Sodasteam".toUpperCase(), "Carmel".toUpperCase(),"Epilady".toUpperCase(), "Ahava".toUpperCase(),
  "Jaffa".toUpperCase(), "Kedem".toUpperCase(), "Coral".toUpperCase(), "Top".toUpperCase(),
  "Beigel".toUpperCase(), "Hasat".toUpperCase(), "Sabra".toUpperCase(), "Osem".toUpperCase(),
  "Dagir".toUpperCase(), "Holyland".toUpperCase(), "Amba".toUpperCase(), "Green Valley".toUpperCase(),
  "Tivall".toUpperCase(), "agrofresh".toUpperCase(), "Jordan Valley".toUpperCase(), "Dana".toUpperCase(),
  // les marques des entreprises qui soutiennent le pays en question
  // Le groupe COCA-COLA
  "COCA-COLA".toUpperCase(), "Aquarius".toUpperCase(),"Cherry Coke".toUpperCase(), "Fanta".toUpperCase(),
  "Nestea".toUpperCase(), "Sprite".toUpperCase(), "Minute Maid".toUpperCase(), "Tropical".toUpperCase(),
  "Hawai".toUpperCase(), "Hawaï".toUpperCase(), "Pom’s".toUpperCase(),
  // Le groupe DANONE
  "DANONE".toUpperCase(), "Arvie".toUpperCase(), "Badoit".toUpperCase(), "Belin".toUpperCase(), "Blédina".toUpperCase(),
  "Bledina".toUpperCase(), "Phosphatine".toUpperCase(), "Chipster".toUpperCase(), "Evian".toUpperCase(), "Galbani".toUpperCase(),
  "Gervais".toUpperCase(), "Heudebert".toUpperCase(), "Lu".toUpperCase(), "Taillefine".toUpperCase(), "Volvic".toUpperCase(),
  // Le groupe NESTLÉ
  "NESTLÉ".toUpperCase(), "NESTLE".toUpperCase(), "Aquarel".toUpperCase(), "Cheerios".toUpperCase(), "Crunch".toUpperCase(),
  "Frigor".toUpperCase(), "Friskies".toUpperCase(), "Galak".toUpperCase(), "Golden Grahams".toUpperCase(), "Kit Kat".toUpperCase(),
  "Maggi".toUpperCase(), "Mousline".toUpperCase(), "Nescafé".toUpperCase(), "Nescafe".toUpperCase(), "Ricoré".toUpperCase(),
  "Ricore".toUpperCase(), "Quality Street".toUpperCase(), "Vittel".toUpperCase(), "Perrier".toUpperCase(), "Buitoni".toUpperCase(),
  // Le groupe INTEL
  "INTEL".toUpperCase(),
  // Le groupe L’ORÉAL
  "L’ORÉAL".toUpperCase(), "L’OREAL".toUpperCase(), "OREAL".toUpperCase(), "ORÉAL".toUpperCase(), "Biotherm".toUpperCase(),
  "Cacharel".toUpperCase(), "Giorgio Armani".toUpperCase(), "Lancôme".toUpperCase(), "Lancome".toUpperCase(), "Vichy".toUpperCase(),
  "La Roche-Posay".toUpperCase(), "Garnier".toUpperCase(), "Héléna Rubinstein".toUpperCase(), "Helena Rubinstein".toUpperCase(),
  "Gemey-Maybelline".toUpperCase(), "Jean-Louis David".toUpperCase(), "Le Club des créateurs de beauté".toUpperCase(),
  "Le Club des createurs de beaute".toUpperCase(), "Redken".toUpperCase(), "Ralph Lauren".toUpperCase(), "Ushuaïa".toUpperCase(),
  "Ushuaia".toUpperCase(),
  // Le groupe ESTÉE LAUDER
  "ESTÉE LAUDER".toUpperCase(), "ESTEE LAUDER".toUpperCase(), "Aramis".toUpperCase(), "Clinique".toUpperCase(), "la Mer".toUpperCase(),
  "DKNY".toUpperCase(), "Tommy Hilfiger".toUpperCase(), "Hilfiger".toUpperCase(),
  // Le groupe DELTA GALIL
  "Marks & Spencers".toUpperCase(), "Carrefour".toUpperCase(), "Tex".toUpperCase(), "Auchan".toUpperCase(), "Gap".toUpperCase(),
  "Hugo Boss".toUpperCase(), "Playtex".toUpperCase(), "Calvin Klein".toUpperCase(), "Victoria’s Secret".toUpperCase(),
  // Le groupe LEVI STRAUSS JEANS ET CELIO
  "LEVI STRAUSS JEANS".toUpperCase(), "CELIO".toUpperCase(),
  // Le groupe TIMBERLAND
  "TIMBERLAND".toUpperCase(),
  // Le groupe DISNEYLAND
  "DISNEYLAND".toUpperCase(),
  // LE groupe NOKIA
  "NOKIA".toUpperCase(),
  // Le groupe MC DONALD’S
  "MC DONALD’S".toUpperCase(), "MC DONALD".toUpperCase(),
  // Le groupe CATERPILLAR
  "CATERPILLAR".toUpperCase(),
  // Le groupe ACCORHOTEL
  "ACCORHOTEL".toUpperCase(),
  // AUTRES
  "Cigarettes Morris".toUpperCase(), "Malboro".toUpperCase(), "Kleenex".toUpperCase(), "Kotex".toUpperCase(), "Huggies".toUpperCase(),
  "SanDisk".toUpperCase(), "Toys RUs".toUpperCase()

];

export const starterBanned: string[] = ["729", "871"];

export const countryBanned: string[] = ["israel"];
